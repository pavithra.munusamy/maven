package com.test;

import org.testng.annotations.AfterClass;
import org.testng.annotations.Test;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

public class NewTest1 {
  @Test(priority=3)
  public void modifyCustomer() {
	  System.out.println("The customer will get modified"); 
  }
  
  @Test(priority=2)
  
  public void createCustomer() {
	  System.out.println("The customer will get created"); 
  }
  
  @Test(priority=1)
  
  public void newCustomer() {
	  System.out.println("New customer will get created"); 
  }
  
  @BeforeMethod
  
  public void beforeCustomer() {
	  System.out.println("Verifying the customer"); 
  }
  
  @AfterMethod
  public void afterCustomer() {
	  System.out.println("All the transactions are done"); 
  }
  
  @BeforeClass
  
  public void beforeClassMethod() {
	  System.out.println("start database connection, Launch Browser");
  }
  
  @AfterClass
  public void afterClassMethod() {
	  System.out.println("Close database connection, Close Browser");
  }
}
