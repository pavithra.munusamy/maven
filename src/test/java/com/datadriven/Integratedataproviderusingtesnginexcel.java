package com.datadriven;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.time.Duration;

import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import jxl.Sheet;
import jxl.Workbook;
import jxl.read.biff.BiffException;

public class Integratedataproviderusingtesnginexcel {
	WebDriver driver;
	
	@BeforeClass
	public void lauch() {
		driver.get("https://demowebshop.tricentis.com/login");
		driver.manage().window().maximize();
		driver.manage().timeouts().implicitlyWait(Duration.ofSeconds(15));
	}
	
		@Test(dataProvider = "test")
		public void excelData(String username, String password) {
			driver.findElement(By.id("Email")).sendKeys(username);
			driver.findElement(By.name("Password")).sendKeys(password);
			driver.findElement(By.xpath("//input[@value='Log in']")).click();
			
			driver.findElement(By.linkText("Log out")).click();
			
		}
		

		
		@DataProvider(name = "test")
		public Object[][] datadriven() throws BiffException, IOException{
			File f=new File("/home/pavithra/Documents/Test_data/Testdata1.xls");
			FileInputStream fis=new FileInputStream(f);
			Workbook book= Workbook.getWorkbook(fis);
			Sheet sh=book.getSheet("Sheet1");
			
			int rows = sh.getRows();
			int columns= sh.getColumns();
			
			Object d[][] = new Object[rows-1][columns];
			
			for(int i=1; i<rows;i++) {
				for(int j=0; j<columns; j++) {
					d[i-1][j] =sh.getCell(j,i).getContents();
			
		}
	
	


			}
			return d;
		}
}

